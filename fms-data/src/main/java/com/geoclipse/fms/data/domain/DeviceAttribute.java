package com.geoclipse.fms.data.domain;

import javax.persistence.*;


/**
 * The persistent class for the attribute database table.
 * Represents a custom field value for a device
 */
@Entity
@DiscriminatorValue(AttributeDiscriminatorValueDef.COMPANY)
public class DeviceAttribute extends AbstractAttribute {
    
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	// bi-directional many-to-one association to Company
    // TODO
    @ManyToOne
	@JoinColumn(name="entity_id", nullable=false)
	private Device device;

	public Device getCompany() {
		return device;
	}

	public void setCompany(Device device) {
		this.device = device;
	}
	
}