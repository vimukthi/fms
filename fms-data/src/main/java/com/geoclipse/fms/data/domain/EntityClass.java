package com.geoclipse.fms.data.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the entity_class database table. Represents a
 * generic entity class in the database
 */
@Entity
@Table(name = "entity_class")
public class EntityClass extends AbstractCommonFieldDefinitionWithKey implements
		Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name = "name", nullable = false, length = 200)
	private String name;

	public EntityClass() {
	}

	/**
	 * @param name
	 */
	public EntityClass(String key, String name) {
		super();
		this.setUniqueKey(key);
		this.name = name;
	}



	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
}