package com.geoclipse.fms.data.domain;

import javax.persistence.*;


/**
 * The persistent class for the attribute database table.
 * 
 */
@Entity
@DiscriminatorValue(AttributeDiscriminatorValueDef.VEHICLE_GROUP)
public class VehicleGroupAttribute extends AbstractAttribute {
    
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	// bi-directional many-to-one association to VehicleGroup
    // TODO
    @ManyToOne
	@JoinColumn(name="entity_id", nullable=false)
	private VehicleGroup vehicleGroup;

	public VehicleGroup getVehicleGroup() {
		return vehicleGroup;
	}

	public void setVehicleGroup(VehicleGroup vehicleGroup) {
		this.vehicleGroup = vehicleGroup;
	}
	
}