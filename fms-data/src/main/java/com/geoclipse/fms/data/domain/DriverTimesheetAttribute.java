package com.geoclipse.fms.data.domain;

import javax.persistence.*;


/**
 * The persistent class for the attribute database table.
 * 
 */
@Entity
@DiscriminatorValue(AttributeDiscriminatorValueDef.DRIVER_TIMESHEET)
public class DriverTimesheetAttribute extends AbstractAttribute {
    
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	// bi-directional many-to-one association to DriverTimesheet
    // TODO
    @ManyToOne
	@JoinColumn(name="entity_id", nullable=false)
	private DriverTimesheet driverTimesheet;

	public DriverTimesheet getDriverTimesheet() {
		return driverTimesheet;
	}

	public void setDriverTimesheet(DriverTimesheet driverTimesheet) {
		this.driverTimesheet = driverTimesheet;
	}
	
}