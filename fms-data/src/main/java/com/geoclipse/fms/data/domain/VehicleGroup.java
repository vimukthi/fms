package com.geoclipse.fms.data.domain;

import java.io.Serializable;
import javax.persistence.*;

import java.util.Set;


/**
 * The persistent class for the vehicle_group database table.
 * 
 */
@Entity
@Table(name="vehicle_group")
public class VehicleGroup extends AbstractCommonFieldDefinitionWithKey implements Serializable {
	private static final long serialVersionUID = 1L;

	//bi-directional many-to-one association to Company
    @ManyToOne
	@JoinColumn(name="company_id", nullable=false)
	private Company company;
    
    @Column(name="name", nullable=false, length=200)
	private String name;

	//bi-directional many-to-one association to Vehicle
	@OneToMany(mappedBy="vehicleGroup")
	private Set<Vehicle> vehicles;

    public VehicleGroup() {
    }

	public Set<Vehicle> getVehicles() {
		return this.vehicles;
	}

	public void setVehicles(Set<Vehicle> vehicles) {
		this.vehicles = vehicles;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}