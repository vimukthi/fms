package com.geoclipse.fms.data.domain;

import javax.persistence.*;


/**
 * The persistent class for the attribute_definition database table.
 * Represents a custom field definition for an entity class
 */
@Entity
@Table(name="attribute_definition")
public class AttributeDefinition extends AbstractCommonFieldDefinitionWithKey {
	private static final long serialVersionUID = 1L;

	@Column(name="name", nullable=false, length=200)
	private String name;

	@Column(name="type", nullable=false, length=200)
	private String type;
	
	// Is this attribute restricted in terms of values it is allowed to take?
	@Column(name="restricted")
	private int restricted;
	
	// Only applies to restricted attributes
	@Column(name="allowed_values", nullable=true)
	private String allowedValues;

	//bi-directional many-to-one association to Company
    @ManyToOne
	@JoinColumn(name="company_id", nullable=false)
	private Company company;
    
    //bi-directional many-to-one association to Company
    @ManyToOne
	@JoinColumn(name="entity_class_id", nullable=false)
	private EntityClass entityClass;

    
    
	/**
	 * @param name
	 * @param type
	 * @param restricted
	 * @param allowedValues
	 * @param company
	 * @param entityClass
	 */
	public AttributeDefinition(String key, String name, String type, int restricted,
			String allowedValues, Company company, EntityClass entityClass) {
		super();
		this.setUniqueKey(key);
		this.name = name;
		this.type = type;
		this.restricted = restricted;
		this.allowedValues = allowedValues;
		this.company = company;
		this.entityClass = entityClass;
	}
	
	public AttributeDefinition(){
		
	}

	public Company getCompany() {
		return this.company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public EntityClass getEntityClass() {
		return entityClass;
	}

	public void setEntityClass(EntityClass entityClass) {
		this.entityClass = entityClass;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getRestricted() {
		return restricted;
	}

	public void setRestricted(int restricted) {
		this.restricted = restricted;
	}

	public String getAllowedValues() {
		return allowedValues;
	}

	public void setAllowedValues(String allowedValues) {
		this.allowedValues = allowedValues;
	}
	
}