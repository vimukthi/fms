package com.geoclipse.fms.data.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;


/**
 * The persistent class for the driver database table.
 * 
 */
@Entity
@Table(name="driver")
public class Driver extends AbstractCommonFieldDefinitionWithKey implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="name", nullable=false, length=200)
	private String name;

	//bi-directional many-to-one association to Company
    @ManyToOne
	@JoinColumn(name="company_id", nullable=false)
	private Company company;

	//bi-directional many-to-one association to DriverTimesheet
	@OneToMany(mappedBy="driver")
	private Set<DriverTimesheet> driverTimesheets;

	//bi-directional many-to-one association to Journey
	@OneToMany(mappedBy="driver")
	private Set<Journey> journeys;

    public Driver() {
    }

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Company getCompany() {
		return this.company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}
	
	public Set<DriverTimesheet> getDriverTimesheets() {
		return this.driverTimesheets;
	}

	public void setDriverTimesheets(Set<DriverTimesheet> driverTimesheets) {
		this.driverTimesheets = driverTimesheets;
	}
	
	public Set<Journey> getJourneys() {
		return this.journeys;
	}

	public void setJourneys(Set<Journey> journeys) {
		this.journeys = journeys;
	}
	
}