package com.geoclipse.fms.data.domain;

import javax.persistence.*;


/**
 * The persistent class for the attribute database table.
 * 
 */
@Entity
@DiscriminatorValue(AttributeDiscriminatorValueDef.LOCATION)
public class LocationAttribute extends AbstractAttribute {
    
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	// bi-directional many-to-one association to Journey
    // TODO
    @ManyToOne
	@JoinColumn(name="entity_id", nullable=false)
	private Location location;

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}
}