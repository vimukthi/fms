package com.geoclipse.fms.data.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;


/**
 * The persistent class for the company database table.
 * Represents a single company in the domain
 */
@Entity
@Table(name="company")
public class Company extends AbstractCommonFieldDefinitionWithKey implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="address", nullable=true, length=1000)
	private String address;

	@Column(name="name", nullable=false, length=200)
	private String name;
	
	@Column(name="description", nullable=true, length=500)
	private String description;

	@Column(name="telephone", nullable=true, length=100)
	private String telephone;
	
	@Column(name="email", nullable=true, length=200)
	private String email;
	
	@Column(name="notify_email", nullable=false, length=200)
	private String notifyEmail;
	
	@Column(name="active")
	private int active;

	//bi-directional many-to-one association to Configuration
	@OneToMany(mappedBy="company", cascade=CascadeType.PERSIST)
	private Set<Configuration> configurations;

	//bi-directional many-to-one association to Device
	@OneToMany(mappedBy="company", cascade=CascadeType.PERSIST)
	private Set<Device> devices;

	//bi-directional many-to-one association to Driver
	@OneToMany(mappedBy="company", cascade=CascadeType.PERSIST)
	private Set<Driver> drivers;

	//bi-directional many-to-one association to Geozone
	@OneToMany(mappedBy="company", cascade=CascadeType.PERSIST)
	private Set<Geozone> geozones;

	//bi-directional many-to-one association to Role
	@OneToMany(mappedBy="company", cascade=CascadeType.PERSIST)
	private Set<Role> roles;

	//bi-directional many-to-one association to Route
	@OneToMany(mappedBy="company", cascade=CascadeType.PERSIST)
	private Set<Route> routes;

	//bi-directional many-to-one association to User
	@OneToMany(mappedBy="company", cascade=CascadeType.PERSIST)
	private Set<User> users;

    public Company() {
    }
    

	/**
	 * @param name
	 * @param notify_email
	 */
	public Company(String name, String notify_email, String uniqueKey) {
		super();
		this.name = name;
		this.notifyEmail = notify_email;
		this.setUniqueKey(uniqueKey);
	}


	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Configuration> getConfigurations() {
		return this.configurations;
	}

	public void setConfigurations(Set<Configuration> configurations) {
		this.configurations = configurations;
	}
	
	public Set<Device> getDevices() {
		return this.devices;
	}

	public void setDevices(Set<Device> devices) {
		this.devices = devices;
	}
	
	public Set<Driver> getDrivers() {
		return this.drivers;
	}

	public void setDrivers(Set<Driver> drivers) {
		this.drivers = drivers;
	}
	
	public Set<Geozone> getGeozones() {
		return this.geozones;
	}

	public void setGeozones(Set<Geozone> geozones) {
		this.geozones = geozones;
	}
	
	public Set<Role> getRoles() {
		return this.roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}
	
	public Set<Route> getRoutes() {
		return this.routes;
	}

	public void setRoutes(Set<Route> routes) {
		this.routes = routes;
	}
	
	public Set<User> getUsers() {
		return this.users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isActive() {
		return active == 1;
	}

	public void setActive(boolean active) {
		this.active = active ? 1 : 0;
	}


	public String getNotifyEmail() {
		return notifyEmail;
	}


	public void setNotifyEmail(String notifyEmail) {
		this.notifyEmail = notifyEmail;
	}	
}