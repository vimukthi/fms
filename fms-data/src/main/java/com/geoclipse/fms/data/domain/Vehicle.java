package com.geoclipse.fms.data.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;


/**
 * The persistent class for the vehicle database table.
 * 
 */
@Entity
@Table(name="vehicle")
public class Vehicle extends AbstractCommonFieldDefinitionWithKey implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="odometer", nullable=false)
	private double odometer;
	
	@OneToOne
	@JoinColumn(name="device_id", unique=true, nullable=false, updatable=false)
	private Device device;

	//bi-directional many-to-one association to Journey
	@OneToMany(mappedBy="vehicle")
	private Set<Journey> journeys;

	//bi-directional many-to-one association to VehicleGroup
    @ManyToOne
	@JoinColumn(name="vehicle_group_id", nullable=false)
	private VehicleGroup vehicleGroup;

	//bi-directional many-to-one association to VehicleModel
    @ManyToOne
	@JoinColumn(name="vehicle_model_id", nullable=false)
	private VehicleModel vehicleModel;

	//bi-directional many-to-one association to VehicleEvent
	@OneToMany(mappedBy="vehicle")
	private Set<VehicleEvent> vehicleEvents;

    public Vehicle() {
    }

	public double getOdometer() {
		return this.odometer;
	}

	public void setOdometer(double odometer) {
		this.odometer = odometer;
	}

	public Set<Journey> getJourneys() {
		return this.journeys;
	}

	public void setJourneys(Set<Journey> journeys) {
		this.journeys = journeys;
	}
	
	public VehicleGroup getVehicleGroup() {
		return this.vehicleGroup;
	}

	public void setVehicleGroup(VehicleGroup vehicleGroup) {
		this.vehicleGroup = vehicleGroup;
	}
	
	public VehicleModel getVehicleModel() {
		return this.vehicleModel;
	}

	public void setVehicleModel(VehicleModel vehicleModel) {
		this.vehicleModel = vehicleModel;
	}
	
	public Set<VehicleEvent> getVehicleEvents() {
		return this.vehicleEvents;
	}

	public void setVehicleEvents(Set<VehicleEvent> vehicleEvents) {
		this.vehicleEvents = vehicleEvents;
	}

	public Device getDevice() {
		return device;
	}

	public void setDevice(Device device) {
		this.device = device;
	}
	
}