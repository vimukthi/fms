package com.geoclipse.fms.data.domain;

import javax.persistence.*;


/**
 * The persistent class for the attribute database table.
 * 
 */
@Entity
@DiscriminatorValue(AttributeDiscriminatorValueDef.VEHICLE_EVENT)
public class VehicleEventAttribute extends AbstractAttribute {
    
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	// bi-directional many-to-one association to VehicleEvent
    // TODO
    @ManyToOne
	@JoinColumn(name="entity_id", nullable=false)
	private VehicleEvent vehicleEvent;

	public VehicleEvent getVehicleEvent() {
		return vehicleEvent;
	}

	public void setVehicleEvent(VehicleEvent vehicleEvent) {
		this.vehicleEvent = vehicleEvent;
	}
	
}