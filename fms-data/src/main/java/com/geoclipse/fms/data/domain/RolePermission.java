package com.geoclipse.fms.data.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the role_permission database table.
 * 
 */
@Entity
@Table(name="role_permission")
public class RolePermission extends AbstractCommonFieldDefinition implements Serializable {
	private static final long serialVersionUID = 1L;

	//bi-directional many-to-one association to Permission
    @ManyToOne
	@JoinColumn(name="permission_id", nullable=false)
	private Permission permission;

	//bi-directional many-to-one association to Role
    @ManyToOne
	@JoinColumn(name="role_id", nullable=false)
	private Role role;

    public RolePermission() {
    }

	public Permission getPermission() {
		return this.permission;
	}

	public void setPermission(Permission permission) {
		this.permission = permission;
	}
	
	public Role getRole() {
		return this.role;
	}

	public void setRole(Role role) {
		this.role = role;
	}
	
}