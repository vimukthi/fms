/**
 * 
 */
package com.geoclipse.fms.data.domain;

/**
 * @author Vimukthi
 * Discriminator values for entities in custom attributes table
 *
 */
public interface AttributeDiscriminatorValueDef {
	
	public static final String COMPANY = "company";
	public static final String DRIVER = "driver";
	public static final String DRIVER_TIMESHEET = "driver_timesheet";
	public static final String GEOZONE = "geozone";
	public static final String JOURNEY = "journey";
	public static final String LOCATION = "location";
	public static final String USER = "user";
	public static final String ROUTE = "route";
	public static final String VEHICLE = "vehicle";
	public static final String VEHICLE_GROUP = "vehicle_group";
	public static final String VEHICLE_EVENT = "vehicle_event";
}
