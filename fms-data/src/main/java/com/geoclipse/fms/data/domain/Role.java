package com.geoclipse.fms.data.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;


/**
 * The persistent class for the role database table.
 * 
 */
@Entity
@Table(name="role")
public class Role extends AbstractCommonFieldDefinitionWithKey implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="name", nullable=false, length=200)
	private String name;

	@Column(name="type", nullable=false, length=200)
	private String type;

	//bi-directional many-to-one association to Company
    @ManyToOne
	@JoinColumn(name="company_id", nullable=false)
	private Company company;

	//bi-directional many-to-one association to Role
    @ManyToOne
	@JoinColumn(name="parent_role_id", nullable=false)
	private Role role;

	//bi-directional many-to-one association to Role
	@OneToMany(mappedBy="role")
	private Set<Role> roles;

	//bi-directional many-to-one association to RolePermission
	@OneToMany(mappedBy="role")
	private Set<RolePermission> rolePermissions;

	//bi-directional many-to-one association to UserRole
	@OneToMany(mappedBy="role")
	private Set<UserRole> userRoles;

    public Role() {
    }

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Company getCompany() {
		return this.company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}
	
	public Role getRole() {
		return this.role;
	}

	public void setRole(Role role) {
		this.role = role;
	}
	
	public Set<Role> getRoles() {
		return this.roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}
	
	public Set<RolePermission> getRolePermissions() {
		return this.rolePermissions;
	}

	public void setRolePermissions(Set<RolePermission> rolePermissions) {
		this.rolePermissions = rolePermissions;
	}
	
	public Set<UserRole> getUserRoles() {
		return this.userRoles;
	}

	public void setUserRoles(Set<UserRole> userRoles) {
		this.userRoles = userRoles;
	}
	
}