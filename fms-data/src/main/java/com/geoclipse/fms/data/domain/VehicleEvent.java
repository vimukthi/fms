package com.geoclipse.fms.data.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the vehicle_event database table.
 * 
 */
@Entity
@Table(name="vehicle_event")
public class VehicleEvent extends AbstractCommonFieldDefinitionWithKey implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="description", nullable=false, length=200)
	private String description;

	@Column(name="type", nullable=false, length=200)
	private String type;

	//bi-directional many-to-one association to Vehicle
    @ManyToOne
	@JoinColumn(name="vehicle_id", nullable=false)
	private Vehicle vehicle;

    public VehicleEvent() {
    }

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Vehicle getVehicle() {
		return this.vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}
	
}