package com.geoclipse.fms.data.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the geozone database table.
 * 
 */
@Entity
@Table(name="geozone")
public class Geozone extends AbstractCommonFieldDefinitionWithKey implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="name", nullable=false, length=200)
	private String type;

	//bi-directional many-to-one association to Company
    @ManyToOne
	@JoinColumn(name="company_id", nullable=false)
	private Company company;

    public Geozone() {
    }

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Company getCompany() {
		return this.company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}
	
}