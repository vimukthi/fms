package com.geoclipse.fms.data.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the journey_stream database table.
 * 
 */
@Entity
@Table(name="journey_stream")
public class JourneyStream extends AbstractCommonFieldDefinitionWithKey implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="type", nullable=false, length=200)
	private String type;

	//bi-directional many-to-one association to Journey
    @ManyToOne
	@JoinColumn(name="journey_id", nullable=false)
	private Journey journey;

    public JourneyStream() {
    }

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Journey getJourney() {
		return this.journey;
	}

	public void setJourney(Journey journey) {
		this.journey = journey;
	}
	
}