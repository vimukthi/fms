package com.geoclipse.fms.data.domain;

import javax.persistence.*;


/**
 * The persistent class for the attribute database table.
 * Represents a custom field value for a company
 */
@Entity
@DiscriminatorValue(AttributeDiscriminatorValueDef.COMPANY)
public class CompanyAttribute extends AbstractAttribute {
    
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	// bi-directional many-to-one association to Company
    // TODO
    @ManyToOne
	@JoinColumn(name="entity_id", nullable=false)
	private Company company;

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}
	
}