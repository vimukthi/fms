/**
 * 
 */
package com.geoclipse.fms.data.attributes;


import com.geoclipse.fms.data.domain.AbstractAttribute;
import com.geoclipse.fms.data.domain.AttributeDefinition;

/**
 * @author Vimukthi
 *
 */
public interface AttributeService<ENTITY, ENTITY_ATTRIBUTE extends AbstractAttribute> {
	
	/**
	 * Add a new attribute for an entity given the {@link AttributeDefinition}
	 * @param company
	 * @param entity
	 * @param attributeName
	 * @param defaultValues
	 */
	public void addAttribute(AttributeDefinition attributeDefinition);
	
	/**
	 * Delete an attribute for an entity given the {@link AttributeDefinition}
	 * @param company
	 * @param entity
	 * @param attributeName
	 */
	public void deleteAttribute(AttributeDefinition attributeDefinition);
	
	/**
	 * Get the attribute specified by the {@link AttributeDefinition} for the given entity instance.
	 * This returns a Object of a class that extends {@link AbstractAttribute}
	 * @param company
	 * @param entity
	 * @param attributeName
	 * @return
	 */
	public ENTITY_ATTRIBUTE getAttribute(ENTITY entity, AttributeDefinition attributeDefinition);
	
	/**
	 * Set the attribute specified by the {@link AttributeDefinition} with given value 
	 * for the given entity instance.
	 * @param company
	 * @param entity
	 * @param attributeName
	 * @param attributeValue
	 */
	public void setAttribute(ENTITY entity, AttributeDefinition attributeDefinition, Object attributeValue);

}
