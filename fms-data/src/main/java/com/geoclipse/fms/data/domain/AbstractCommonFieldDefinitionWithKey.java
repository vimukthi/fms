/**
 * 
 */
package com.geoclipse.fms.data.domain;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

/**
 * @author Vimukthi
 * Common fields for most of the entities with key
 *
 */
@MappedSuperclass
public abstract class AbstractCommonFieldDefinitionWithKey extends AbstractCommonFieldDefinition {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name="unique_key", nullable=false, length=200, unique=true)
	private String uniqueKey;

	public String getUniqueKey() {
		return this.uniqueKey;
	}

	public void setUniqueKey(String uniqueKey) {
		this.uniqueKey = uniqueKey;
	}

}
