/**
 * 
 */
package com.geoclipse.fms.data.attributes;

import javax.persistence.EntityManager;

import com.geoclipse.fms.data.domain.AttributeDefinition;
import com.geoclipse.fms.data.domain.Company;
import com.geoclipse.fms.data.domain.CompanyAttribute;

/**
 * @author Vimukthi
 * 
 */
public class CompanyAttributeService implements
		AttributeService<Company, CompanyAttribute> {

	private EntityManager manager;	

	/**
	 * 
	 */
	public CompanyAttributeService() {
		super();
	}

	/**
	 * @param manager
	 */
	public CompanyAttributeService(EntityManager manager) {
		super();
		this.manager = manager;
	}

	public void addAttribute(AttributeDefinition attributeDefinition) {
		manager.persist(attributeDefinition);
	}

	public void deleteAttribute(AttributeDefinition attributeDefinition) {
		// TODO Auto-generated method stub

	}

	public CompanyAttribute getAttribute(Company entity,
			AttributeDefinition attributeDefinition) {
		// TODO Auto-generated method stub
		return null;
	}

	public void setAttribute(Company entity,
			AttributeDefinition attributeDefinition, Object attributeValue) {
		// TODO Auto-generated method stub

	}

	public EntityManager getManager() {
		return manager;
	}

	public void setManager(EntityManager manager) {
		this.manager = manager;
	}

}
