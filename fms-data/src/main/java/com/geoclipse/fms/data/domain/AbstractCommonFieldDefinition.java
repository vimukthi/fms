/**
 * 
 */
package com.geoclipse.fms.data.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author Vimukthi
 * Common fields for most of the entities
 *
 */
@MappedSuperclass
public abstract class AbstractCommonFieldDefinition implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id", nullable=false)
	private int id;
	
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="date_created", nullable=false)
	private Date dateCreated;

	@Column(name="date_modified", nullable=false)
	private Timestamp dateModified;
	
	

	/**
	 * @param id
	 * @param dateCreated
	 * @param dateModified
	 */
	public AbstractCommonFieldDefinition() {
		super();
		Date now = new Date();
		this.dateCreated = now;
		this.dateModified = new Timestamp(now.getTime());
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	
	public void setDateCreatedNow() {
		this.dateCreated = new Date();
	}

	public Timestamp getDateModified() {
		return dateModified;
	}

	public void setDateModified(Timestamp dateModified) {
		this.dateModified = dateModified;
	}
	
	public void setDateModifiedNow() {
		this.dateModified = new Timestamp(new Date().getTime());
	}

}
