package com.geoclipse.fms.data.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Set;


/**
 * The persistent class for the journey database table.
 * 
 */
@Entity
@Table(name="journey")
public class Journey extends AbstractCommonFieldDefinitionWithKey implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="end_latitude")
	private double endLatitude;

	@Column(name="end_longtitude")
	private double endLongtitude;

	@Column(name="end_time")
	private Timestamp endTime;

	@Column(name="start_latitude")
	private double startLatitude;

	@Column(name="start_longtitude")
	private double startLongtitude;

	@Column(name="start_time")
	private Timestamp startTime;

	//bi-directional many-to-one association to DriverTimesheet
	@OneToMany(mappedBy="journey")
	private Set<DriverTimesheet> driverTimesheets;

	//bi-directional many-to-one association to Driver
    @ManyToOne
	@JoinColumn(name="driver_id", nullable=false)
	private Driver driver;

	//bi-directional many-to-one association to Vehicle
    @ManyToOne
	@JoinColumn(name="vehicle_id", nullable=false)
	private Vehicle vehicle;

	//bi-directional many-to-one association to JourneyStream
	@OneToMany(mappedBy="journey")
	private Set<JourneyStream> journeyStreams;

	//bi-directional many-to-one association to Location
	@OneToMany(mappedBy="journey")
	private Set<Location> locations;

    public Journey() {
    }

	public double getEndLatitude() {
		return this.endLatitude;
	}

	public void setEndLatitude(double endLatitude) {
		this.endLatitude = endLatitude;
	}

	public double getEndLongtitude() {
		return this.endLongtitude;
	}

	public void setEndLongtitude(double endLongtitude) {
		this.endLongtitude = endLongtitude;
	}

	public Timestamp getEndTime() {
		return this.endTime;
	}

	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}

	public double getStartLatitude() {
		return this.startLatitude;
	}

	public void setStartLatitude(double startLatitude) {
		this.startLatitude = startLatitude;
	}

	public double getStartLongtitude() {
		return this.startLongtitude;
	}

	public void setStartLongtitude(double startLongtitude) {
		this.startLongtitude = startLongtitude;
	}

	public Timestamp getStartTime() {
		return this.startTime;
	}

	public void setStartTime(Timestamp startTime) {
		this.startTime = startTime;
	}

	public Set<DriverTimesheet> getDriverTimesheets() {
		return this.driverTimesheets;
	}

	public void setDriverTimesheets(Set<DriverTimesheet> driverTimesheets) {
		this.driverTimesheets = driverTimesheets;
	}
	
	public Driver getDriver() {
		return this.driver;
	}

	public void setDriver(Driver driver) {
		this.driver = driver;
	}
	
	public Vehicle getVehicle() {
		return this.vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}
	
	public Set<JourneyStream> getJourneyStreams() {
		return this.journeyStreams;
	}

	public void setJourneyStreams(Set<JourneyStream> journeyStreams) {
		this.journeyStreams = journeyStreams;
	}
	
	public Set<Location> getLocations() {
		return this.locations;
	}

	public void setLocations(Set<Location> locations) {
		this.locations = locations;
	}
	
}