package com.geoclipse.fms.data.domain;

import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;


/**
 * The persistent class for the location database table.
 * 
 */
@Entity
@Table(name="location")
public class Location extends AbstractCommonFieldDefinitionWithKey implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="altitude")
	private double altitude;

	@Column(name="distance_km")
	private double distanceKm;

	@Column(name="fuel_level")
	private double fuelLevel;

	@Column(name="heading")
	private double heading;

	@Column(name="latitude")
	private double latitude;

	@Column(name="longitude")
	private double longitude;

	@Column(name="odometer_km")
	private double odometerKm;

	@Column(name="speed_kmph")
	private double speedKmph;

	@Column(name="status_code", nullable=false)
	private int statusCode;

	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="time", nullable=false)
	private Date time;

	//bi-directional many-to-one association to Geozone
    @ManyToOne
	@JoinColumn(name="geozone_id", nullable=false)
	private Geozone geozone;

	//bi-directional many-to-one association to Journey
    @ManyToOne
	@JoinColumn(name="journey_id", nullable=false)
	private Journey journey;

    public Location() {
    }

	public double getAltitude() {
		return this.altitude;
	}

	public void setAltitude(double altitude) {
		this.altitude = altitude;
	}

	public double getDistanceKm() {
		return this.distanceKm;
	}

	public void setDistanceKm(double distanceKm) {
		this.distanceKm = distanceKm;
	}

	public double getFuelLevel() {
		return this.fuelLevel;
	}

	public void setFuelLevel(double fuelLevel) {
		this.fuelLevel = fuelLevel;
	}

	public double getHeading() {
		return this.heading;
	}

	public void setHeading(double heading) {
		this.heading = heading;
	}

	public double getLatitude() {
		return this.latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return this.longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getOdometerKm() {
		return this.odometerKm;
	}

	public void setOdometerKm(double odometerKm) {
		this.odometerKm = odometerKm;
	}

	public double getSpeedKmph() {
		return this.speedKmph;
	}

	public void setSpeedKmph(double speedKmph) {
		this.speedKmph = speedKmph;
	}

	public int getStatusCode() {
		return this.statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public Date getTime() {
		return this.time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public Geozone getGeozone() {
		return this.geozone;
	}

	public void setGeozone(Geozone geozone) {
		this.geozone = geozone;
	}
	
	public Journey getJourney() {
		return this.journey;
	}

	public void setJourney(Journey journey) {
		this.journey = journey;
	}
	
}