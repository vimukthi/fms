package com.geoclipse.fms.data.domain;

import javax.persistence.*;


/**
 * The persistent class for the attribute database table.
 * 
 */
@Entity
@DiscriminatorValue(AttributeDiscriminatorValueDef.GEOZONE)
public class GeozoneAttribute extends AbstractAttribute {
    
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	// bi-directional many-to-one association to Geozone
    // TODO
    @ManyToOne
	@JoinColumn(name="entity_id", nullable=false)
	private Geozone geozone;

	public Geozone getGeozone() {
		return geozone;
	}

	public void setGeozone(Geozone geozone) {
		this.geozone = geozone;
	}
	
}