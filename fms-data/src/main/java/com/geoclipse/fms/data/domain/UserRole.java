package com.geoclipse.fms.data.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the user_role database table.
 * 
 */
@Entity
@Table(name="user_role")
public class UserRole extends AbstractCommonFieldDefinition implements Serializable {
	private static final long serialVersionUID = 1L;

	//bi-directional many-to-one association to Role
    @ManyToOne
	@JoinColumn(name="role_id", nullable=false)
	private Role role;

	//bi-directional many-to-one association to User
    @ManyToOne
	@JoinColumn(name="user_id", nullable=false)
	private User user;

    public UserRole() {
    }

	public Role getRole() {
		return this.role;
	}

	public void setRole(Role role) {
		this.role = role;
	}
	
	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
}