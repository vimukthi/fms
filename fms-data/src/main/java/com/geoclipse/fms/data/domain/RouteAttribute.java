package com.geoclipse.fms.data.domain;

import javax.persistence.*;


/**
 * The persistent class for the attribute database table.
 * 
 */
@Entity
@DiscriminatorValue(AttributeDiscriminatorValueDef.ROUTE)
public class RouteAttribute extends AbstractAttribute {
    
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	// bi-directional many-to-one association to Route
    // TODO
    @ManyToOne
	@JoinColumn(name="entity_id", nullable=false)
	private Route route;

	public Route getRoute() {
		return route;
	}

	public void setRoute(Route route) {
		this.route = route;
	}
	
}