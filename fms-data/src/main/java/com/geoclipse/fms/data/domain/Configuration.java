package com.geoclipse.fms.data.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the configuration database table.
 * A single configuration value for an entity
 */
@Entity
@Table(name="configuration")
public class Configuration extends AbstractCommonFieldDefinition implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="name", nullable=false, length=200)
	private String name;

	@Column(name="type", nullable=false, length=200)
	private String type;

	@Column(name="value", nullable=false, length=200)
	private String value;

	//bi-directional many-to-one association to Company
    @ManyToOne
	@JoinColumn(name="company_id", nullable=false)
	private Company company;

    public Configuration() {
    }    

	/**
	 * @param name
	 * @param type
	 * @param value
	 * @param company
	 */
	public Configuration(String name, String type, String value, Company company) {
		super();
		this.name = name;
		this.type = type;
		this.value = value;
		this.company = company;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Company getCompany() {
		return this.company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}