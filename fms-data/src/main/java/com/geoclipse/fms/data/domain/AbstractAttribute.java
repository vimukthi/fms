package com.geoclipse.fms.data.domain;

import javax.persistence.*;


/**
 * The persistent class for the attribute database table.
 * This class captures some of the common fields for custom attributes of entities
 * 
 */
@Entity
@Table(name="attribute")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="entity_type", discriminatorType=DiscriminatorType.STRING, length=200)
public abstract class AbstractAttribute extends AbstractCommonFieldDefinition {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	// many-to-one association to AttributeDefinition
    @ManyToOne
	@JoinColumn(name="attribute_definition_id", nullable=false)
	protected AttributeDefinition attributeDefinition;

	@Column(name="str_value", nullable=false, length=2000)
	protected String strValue;
	
	@Column(name="int_value")
	protected int intValue;
	
	@Column(name="float_value")
	protected Double floatValue;

	public Double getFloatValue() {
		return floatValue;
	}

	public void setFloatValue(Double floatValue) {
		this.floatValue = floatValue;
	}

	public AttributeDefinition getAttributeDefinition() {
		return attributeDefinition;
	}

	public void setAttributeDefinition(AttributeDefinition attributeDefinition) {
		this.attributeDefinition = attributeDefinition;
	}

	public String getStrValue() {
		return strValue;
	}

	public void setStrValue(String strValue) {
		this.strValue = strValue;
	}

	public int getIntValue() {
		return intValue;
	}

	public void setIntValue(int intValue) {
		this.intValue = intValue;
	}	
}