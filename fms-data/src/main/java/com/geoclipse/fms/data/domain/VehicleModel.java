package com.geoclipse.fms.data.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;


/**
 * The persistent class for the vehicle_model database table.
 * 
 */
@Entity
@Table(name="vehicle_model")
public class VehicleModel extends AbstractCommonFieldDefinitionWithKey implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="make", nullable=false, length=200)
	private String make;

	@Column(name="model", nullable=false, length=200)
	private String model;

	@Column(name="type", nullable=false, length=200)
	private String type;

	@Column(name="year", nullable=false, length=200)
	private String year;

	//bi-directional many-to-one association to Vehicle
	@OneToMany(mappedBy="vehicleModel")
	private Set<Vehicle> vehicles;

    public VehicleModel() {
    }

	public String getMake() {
		return this.make;
	}

	public void setMake(String make) {
		this.make = make;
	}

	public String getModel() {
		return this.model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getYear() {
		return this.year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public Set<Vehicle> getVehicles() {
		return this.vehicles;
	}

	public void setVehicles(Set<Vehicle> vehicles) {
		this.vehicles = vehicles;
	}
	
}