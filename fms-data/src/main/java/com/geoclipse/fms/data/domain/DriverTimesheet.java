package com.geoclipse.fms.data.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the driver_timesheet database table.
 * 
 */
@Entity
@Table(name="driver_timesheet")
public class DriverTimesheet extends AbstractCommonFieldDefinition implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="comment", nullable=false, length=200)
	private String comment;

	//bi-directional many-to-one association to Driver
    @ManyToOne
	@JoinColumn(name="driver_id", nullable=false)
	private Driver driver;

	//bi-directional many-to-one association to Journey
    @ManyToOne
	@JoinColumn(name="journey_id", nullable=false)
	private Journey journey;

    public DriverTimesheet() {
    }

	public String getComment() {
		return this.comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Driver getDriver() {
		return this.driver;
	}

	public void setDriver(Driver driver) {
		this.driver = driver;
	}
	
	public Journey getJourney() {
		return this.journey;
	}

	public void setJourney(Journey journey) {
		this.journey = journey;
	}
	
}