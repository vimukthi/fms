/**
 * 
 */
package com.geoclipse.fms.data.tests;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import org.testng.Assert;
import org.testng.annotations.*;

import com.geoclipse.fms.data.attributes.CompanyAttributeService;
import com.geoclipse.fms.data.domain.AttributeDefinition;
import com.geoclipse.fms.data.domain.Company;
import com.geoclipse.fms.data.domain.EntityClass;

/**
 * @author Vimukthi Test class for the Company entity
 */
public class CompanyTest {

	private EntityManager manager;
	
	private Company testCompany;
	
	private CompanyAttributeService attService;

	@BeforeClass
	public void setUp() {
		EntityManagerFactory factory = Persistence
				.createEntityManagerFactory("persistenceUnit");
		manager = factory.createEntityManager();
		attService = new CompanyAttributeService(manager);
	}
	
	@AfterClass
	public void tearDown(){
		manager.close();
	}

	@Test(groups = { "dao" })
	public void daoTest() {
		//System.out.println("DAO test");
	}

	@Test(groups = { "entity" })
	public void entityTest() {
		EntityTransaction tx = manager.getTransaction();
		int numCompanies = manager.createQuery("Select a From Company a", Company.class).getResultList().size();
		tx.begin();
		try {
			testCompany = new Company();
			testCompany.setName("nice");
			testCompany.setUniqueKey("nice");
			testCompany.setAddress("test");
			manager.persist(testCompany);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		tx.commit();
		manager.refresh(testCompany);
		int numCompaniesNow = manager.createQuery("Select a From Company a", Company.class).getResultList().size();		
		Assert.assertEquals(numCompanies + 1, numCompaniesNow);		
		System.out.println("---------------  " + testCompany.getId());
	}

	@Test(groups = { "custom_attributes" })
	public void attributeTest() {
		EntityTransaction tx = manager.getTransaction();
		tx.begin();
		try {
			Company com = new Company();
			com.setName("nice");
			com.setUniqueKey("nice");
			com.setAddress("test");
			manager.persist(com);	
			EntityClass c = new EntityClass("company", "company");
			manager.persist(c);
			AttributeDefinition d = new AttributeDefinition("testAtt", "test Att", "int", 0, "", com, c);
			attService.addAttribute(d);
			//manager.persist(d);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		tx.commit();
	}

	public EntityManager getManager() {
		return manager;
	}

	public void setManager(EntityManager manager) {
		this.manager = manager;
	}

}
