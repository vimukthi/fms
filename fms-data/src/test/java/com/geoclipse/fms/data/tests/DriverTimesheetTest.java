/**
 * 
 */
package com.geoclipse.fms.data.tests;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.testng.annotations.*;

/**
 * @author Vimukthi 
 * Test class for the DriverTimesheet entity
 */
public class DriverTimesheetTest {

	private EntityManager manager;

	@BeforeClass
	public void setUp() {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("persistenceUnit");
		manager = factory.createEntityManager();
	}

	@Test(groups = { "dao" })
	public void daoTest() {
		System.out.println("DAO test");
	}

	@Test(groups = { "entity" })
	public void entityTest() {
		System.out.println("attribute test");
	}

	@Test(groups = { "custom_attributes" })
	public void atttributeTest() {
		System.out.println("attribute test");
	}

	public EntityManager getManager() {
		return manager;
	}

	public void setManager(EntityManager manager) {
		this.manager = manager;
	}

}
