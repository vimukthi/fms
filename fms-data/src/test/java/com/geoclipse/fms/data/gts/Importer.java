/**
 * 
 */
package com.geoclipse.fms.data.gts;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;

import com.geoclipse.fms.data.domain.Company;
import com.geoclipse.fms.data.domain.Configuration;

/**
 * @author Vimukthi
 *
 */
public class Importer {

	private EntityManager manager;
	
	private DataSource ds;
	
	private JdbcTemplate jdbc;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Importer im = new Importer();		
		EntityManagerFactory factory = Persistence
				.createEntityManagerFactory("persistenceUnit");		
		im.setManager(factory.createEntityManager());
		im.setDs();
		// lets start work
		im.importCompanies();
		
	}
	
	public void importCompanies(){		
		EntityTransaction tx = manager.getTransaction();		
		tx.begin();
		try {
			this.getJdbc().query("select * from account", new RowCallbackHandler() {
				
				public void processRow(ResultSet rs) throws SQLException {
					Company c = new Company();
					c.setName(rs.getString("accountID"));
					c.setUniqueKey(rs.getString("accountID"));
					c.setActive(rs.getBoolean("isActive"));
					c.setTelephone(rs.getString("contactPhone"));
					c.setEmail(rs.getString("contactEmail"));
					c.setNotifyEmail(rs.getString("notifyEmail"));
					c.setDescription(rs.getString("description"));
					c.setAddress(rs.getString("description"));
					Set<Configuration> configurations = new HashSet<Configuration>();
					configurations.add(new Configuration("timeZone", "String", rs.getString("timeZone"), c));
					configurations.add(new Configuration("speedUnits", "int", rs.getString("speedUnits"), c));
					configurations.add(new Configuration("distanceUnits", "int", rs.getString("distanceUnits"), c));
					configurations.add(new Configuration("volumeUnits", "int", rs.getString("volumeUnits"), c));
					configurations.add(new Configuration("pressureUnits", "int", rs.getString("pressureUnits"), c));
					configurations.add(new Configuration("economyUnits", "int", rs.getString("economyUnits"), c));
					configurations.add(new Configuration("temperatureUnits", "int", rs.getString("temperatureUnits"), c));
					configurations.add(new Configuration("latLonFormat", "int", rs.getString("latLonFormat"), c));
					configurations.add(new Configuration("geocoderMode", "int", rs.getString("geocoderMode"), c));
					configurations.add(new Configuration("privateLabelName", "int", rs.getString("privateLabelName"), c));
					configurations.add(new Configuration("isBorderCrossing", "int", rs.getString("isBorderCrossing"), c));
					configurations.add(new Configuration("retainedEventAge", "int", rs.getString("retainedEventAge"), c));
					configurations.add(new Configuration("maximumDevices", "int", rs.getString("maximumDevices"), c));
					configurations.add(new Configuration("totalPingCount", "int", rs.getString("totalPingCount"), c));
					configurations.add(new Configuration("maxPingCount", "int", rs.getString("maxPingCount"), c));
					configurations.add(new Configuration("autoAddDevices", "int", rs.getString("autoAddDevices"), c));
					configurations.add(new Configuration("dcsPropertiesID", "String", rs.getString("dcsPropertiesID"), c));
					configurations.add(new Configuration("smsEnabled", "int", rs.getString("smsEnabled"), c));
					configurations.add(new Configuration("smsProperties", "String", rs.getString("smsProperties"), c));
					configurations.add(new Configuration("emailProperties", "String", rs.getString("emailProperties"), c));
					configurations.add(new Configuration("expirationTime", "int", rs.getString("expirationTime"), c));
					configurations.add(new Configuration("defaultUser", "String", rs.getString("defaultUser"), c));
					c.setConfigurations(configurations);
					manager.persist(c);
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}		
		tx.commit();
		int numCompanies = manager.createQuery("Select a From Company a", Company.class).getResultList().size();
		System.out.println("num companies - " + numCompanies);
	}	
	
	public DataSource getDs() {
		return ds;
	}

	public void setDs() {
		PoolProperties p = new PoolProperties();
        p.setUrl("jdbc:mysql://localhost:3306/gts_20130623");
        p.setDriverClassName("com.mysql.jdbc.Driver");
        p.setUsername("root");
        p.setPassword("");
        p.setTestOnBorrow(true);
        p.setValidationQuery("SELECT 1");
        p.setTestOnReturn(false);
        p.setValidationInterval(30000);
        p.setTimeBetweenEvictionRunsMillis(30000);
        p.setMaxActive(100);
        p.setInitialSize(10);
        p.setMaxWait(10000);
        p.setRemoveAbandonedTimeout(60);
        p.setMinEvictableIdleTimeMillis(30000);
        p.setMinIdle(10);
        p.setLogAbandoned(true);
        p.setRemoveAbandoned(true);
        p.setJdbcInterceptors("org.apache.tomcat.jdbc.pool.interceptor.ConnectionState;"+
          "org.apache.tomcat.jdbc.pool.interceptor.StatementFinalizer");
        DataSource datasource = new DataSource();
        datasource.setPoolProperties(p); 
        this.ds = datasource;
        this.setJdbc(new JdbcTemplate(datasource));
	}

	public EntityManager getManager() {
		return manager;
	}

	public void setManager(EntityManager manager) {
		this.manager = manager;
	}
	
	public JdbcTemplate getJdbc() {
		return jdbc;
	}

	public void setJdbc(JdbcTemplate jdbc) {
		this.jdbc = jdbc;
	}

	protected void finalize( ) throws Throwable {
		manager.close();
		ds.close();
	}

}
